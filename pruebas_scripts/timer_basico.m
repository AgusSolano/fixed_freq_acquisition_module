%t = timer('TimerFcn',@(x,y)disp('Hello World!'),'StartDelay',5);


%Create a timer object.

t = timer('StartDelay', 4, 'Period', 2, 'TasksToExecute', inf, 'ExecutionMode', 'fixedRate');

%Specify the value of the StartFcn callback. Note that the example specifies the value in a cell array because the callback function needs to access arguments passed to it:

t.StartFcn = {@my_callback_fcn, 'My start message'};

%Specify the value of the StopFcn callback. Again, the value is specified in a cell array because the callback function needs to access the arguments passed to it:

t.StopFcn = { @my_callback_fcn, 'My stop message'};

%Specify the value of the TimerFcn callback. The example specifies the MATLAB commands in a character vector:

t.TimerFcn = @(x,y)disp('Hello World!');

%Start the timer object:

start(t)


function my_callback_fcn(obj, event, text_arg)

txt1 = ' event occurred at ';
txt2 = text_arg;

event_type = event.Type;
event_time = datestr(event.Data.time);

msg = [event_type txt1 event_time];
disp(msg)
disp(txt2)
end

%https://www.mathworks.com/matlabcentral/answers/284202-how-to-plot-a-real-time-signal-with-axes-automatically-updating