clear all
close all
clc


%Variables Gobales
mainFig = figure;
data.newtime = 0; %new sample flag
data.puntos = 0; %numbers array
guidata(mainFig,data);


%% Create a timer object.

t = timer('StartDelay', 4, 'Period', 2, 'TasksToExecute', inf, 'ExecutionMode', 'fixedRate');
%t.StartFcn = {@my_callback_fcn,mainFig};
%t.StopFcn = { @my_callback_fcn,mainFig};
t.TimerFcn = { @my_callback_fcn,mainFig};
start(t)

%% Main struct

while(1)
   
    data = guidata(mainFig);
    
    if data.newtime == 1
    
        disp(data.puntos)
        data.newtime = 0;
    
    end
    
    guidata(mainFig,data);
    
end


%% Funciones

function my_callback_fcn(obj, event,figHandler)

globalData = guidata(figHandler);

globalData.newtime = 1;
globalData.puntos = [globalData.puntos,globalData.puntos(end)+1];

guidata(figHandler,globalData);

end

%https://www.mathworks.com/matlabcentral/answers/284202-how-to-plot-a-real-time-signal-with-axes-automatically-updating